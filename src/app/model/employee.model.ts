export class Employee {
    constructor(
        public userName: string,
        public firstName: string,
        public lastName: string,
        public email: string,
        public birthDate: Date,
        public basicSalary: number,
        public status: string,
        public group: string,
        public description: Date
    ) {}
}