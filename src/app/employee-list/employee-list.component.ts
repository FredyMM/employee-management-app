import { Component } from '@angular/core';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent {

  constructor() {}

  // Fungsi untuk mengaktifkan dan menonaktifkan sidenav
  toggleNav() {
    const sidenav = document.querySelector<HTMLDivElement>('#mySidenav');
    const animatedIcon = document.querySelector('.animated-icon');

    if (sidenav && (sidenav.style.width === '0px' || sidenav.style.width === '')) {
      sidenav.style.width = '250px';
      animatedIcon?.classList.toggle('open');
    } else if (sidenav) {
      sidenav.style.width = '0px';
      animatedIcon?.classList.toggle('open');
    }
  }
}
