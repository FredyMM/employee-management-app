import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginModel } from 'src/app/model/login.model';
import { AuthService } from '../auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  user: LoginModel = new LoginModel('', '', '');
  errorMessage: string = '';

  showPassword: boolean = false;

  constructor(private authService: AuthService, private router: Router) {}

  onSubmit(): void {
    if (this.authService.isValidUser(this.user)) {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Success Login',
        showConfirmButton: false,
        timer: 1500
      })
      // untuk melakukan navigasi ke halaman employee-list setelah login sukses
      this.router.navigate(['/employee-list']);
    } else {
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Login Failed',
        showConfirmButton: false,
        timer: 1500
      })
      this.errorMessage = '*Username or password is incorrect.';
    }
  }

}
