import { Injectable } from '@angular/core';
import { LoginModel } from '../model/login.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private users : LoginModel[] = [
    { email: 'admin@app.com', username: 'admin', password: 'password' },
    { email: 'fredy@app.com', username: 'fredy', password: 'fredy123' }
  ];

  constructor() { }

  registerUser(user: LoginModel): string | null {
    if (this.isUserRegisteredByEmail(user.email)) {
      if (this.isUserRegisteredByUsername(user.username)) {
        return '*Email and Username are already registered.';
      } else {
        return '*Email is already registered.';
      }
    }

    if (this.isUserRegisteredByUsername(user.username)) {
      return '*Username is already registered.';
    }

    // untuk menyimpan akun pengguna baru ke array users
    this.users.push(user);
    return null;
  }

  isUserRegisteredByEmail(email: string): boolean {
    // untuk memeriksa apakah email sudah terdaftar
    return this.users.some(user => user.email === email);
  }

  isUserRegisteredByUsername(username: string): boolean {
    // untuk memeriksa apakah username sudah terdaftar
    return this.users.some(user => user.username === username);
  }

  isValidUser(user: LoginModel): boolean {
    // untuk memeriksa apakah email dan password valid
    const validUser = this.users.find(u => u.username === user.username);
    return !!validUser && validUser.password === user.password;
  }
}
