import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginModel } from 'src/app/model/login.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  user: LoginModel = new LoginModel('', '', '');
  errorMessage: string | null = null;

  constructor(private authService: AuthService, private router: Router) {}

  onSubmit(): void {
    const registrationResult = this.authService.registerUser(this.user);

    if (registrationResult === null) {
      // Navigasi ke halaman login setelah registrasi sukses
      this.router.navigate(['/login']);
    } else {
      // Menampilkan pesan kesalahan jika registrasi gagal
      this.errorMessage = registrationResult;
    }
  }

}
